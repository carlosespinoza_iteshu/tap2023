﻿namespace Ejemplo2
{
    public partial class MainPage : ContentPage
    {
        float num1=0,num2=0;
        public MainPage()
        {
            InitializeComponent();
        }

        private void btnSuma_Clicked(object sender, EventArgs e)
        {
            if (VerificaValores())
            {
                
                float resultado = num1 + num2;

                DisplayAlert("Resultado", $"La suma es {resultado}", "Ok");
            }
        }

        private void btnResta_Clicked(object sender, EventArgs e)
        {
            if (VerificaValores())
            {
                
                float resultado = num1 - num2;

                DisplayAlert("Resultado", $"La resta es {resultado}", "Ok");
            }
        }

        private bool VerificaValores()
        {
            if (string.IsNullOrEmpty(entNumero1.Text))
            {
                DisplayAlert("Error", "Coloque el numero 1", "Ok");
                return false;
            }
            else
            {
                if (string.IsNullOrEmpty(entNumero2.Text))
                {
                    DisplayAlert("Error", "Coloque el numero 2", "Ok");
                    return false;
                }
                else
                {
                    if(float.TryParse(entNumero1.Text,out num1))
                    {
                        if(float.TryParse(entNumero2.Text,out num2)){
                            return true;
                        }
                        else
                        {
                            DisplayAlert("Error", "Error en el numero 2", "Ok");
                            return false;
                        }
                    }
                    else
                    {
                        DisplayAlert("Error", "Error en el numero 1", "Ok");
                        return false;
                    }
                }
            }
        }
    }
}