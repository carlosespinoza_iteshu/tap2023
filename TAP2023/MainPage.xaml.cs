﻿namespace TAP2023;

public partial class MainPage : ContentPage
{
	int count = 0;

	public MainPage()
	{
		InitializeComponent();
	}

    private void Button_Clicked(object sender, EventArgs e)
    {
		count++;
		lblCelda1.Text = $"Ya presionaste el botón {count} veces";
    }
}

