﻿namespace Pizzeria
{
    public partial class MainPage : ContentPage
    {

        public MainPage()
        {
            InitializeComponent();
        }

        private void Calcular()
        {
            int total = 0;
            //tamaño
            switch (pkrTamanio.SelectedItem.ToString())
            {
                case "Chica $100.00":
                    total += 100;
                    break;
                case "Mediana $150.00":
                    total += 150;
                    break;
                default:
                    break;
            }
            //ingredientes
            int ingredientes = 0;
            if (swtJamon.IsToggled) { ingredientes++; }
            if (swtPina.IsToggled) { ingredientes++; }
            if (swtPepperoni.IsToggled) { ingredientes++; }
            if (ingredientes > 2)
            {
                total += (ingredientes - 2) * 20;
            }

            //Complementos
            if(swtCocacola.IsToggled) { total += 40; }
            if(swtPalitosPan.IsToggled) { total += 120; }
            if(swtPapasGajo.IsToggled) { total += 50; }

            //establecer total
            lblTotal.Text = total.ToString();
        }

        private void pkrTamanio_SelectedIndexChanged(object sender, EventArgs e)
        {
            Calcular();
        }

        private void swtPepperoni_Toggled(object sender, ToggledEventArgs e)
        {
            Calcular();
        }
    }
}